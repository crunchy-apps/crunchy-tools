package crunchyTools

import (
	"github.com/kardianos/osext"
	"io/ioutil"
	"os"
)

func GetApplicationRootFolder() string {
	actualPath, err := osext.ExecutableFolder()
	HasError(err, "Crunchy-Tools - GetApplicationRootFolder - ExecutableFolder", false)
	return actualPath
}

func StringToFile(source string, fileName string, permission os.FileMode) error {
	sourceBytes := []byte(source)
	errWriteFile := ioutil.WriteFile(fileName, sourceBytes, permission)
	return HasError(errWriteFile, "Crunchy-Tools - FileHelpers - StringToFile", false)
}

func ByteToFile(source []byte, fileName string, permission os.FileMode) error {
	errWriteFile := ioutil.WriteFile(fileName, source, permission)
	return HasError(errWriteFile, "Crunchy-Tools - FileHelpers - StringToFile", false)
}
